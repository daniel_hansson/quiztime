//
//  ViewController.m
//  QuizTime
//
//  Created by Daniel Hansson on 2016-02-02.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "ViewController.h"
#import "QuestionHandler.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *questionText;
@property (weak, nonatomic) IBOutlet UIButton *answer1;
@property (weak, nonatomic) IBOutlet UIButton *answer2;
@property (weak, nonatomic) IBOutlet UIButton *answer3;
@property (weak, nonatomic) IBOutlet UIButton *answer4;
@property (weak, nonatomic) IBOutlet UILabel *message;
@property (strong, nonatomic) QuestionHandler *questionHandler;
@property (weak, nonatomic) IBOutlet UIButton *nextQuestion;
@property (weak, nonatomic) IBOutlet UIButton *playANewGame;


@end

@implementation ViewController

-(QuestionHandler*)questionHandler{
    if (!_questionHandler) {
        _questionHandler = [[QuestionHandler alloc] init];
    }
    return _questionHandler;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self newQuestion:(id) nil];
    self.playANewGame.hidden = YES;
}

- (IBAction)answerQuestion:(id)sender {
    if([self.questionHandler isAnswerCorrect:[(UIButton *)sender currentTitle]]){
        self.message.text = @"Correct!";
        self.message.textColor = [UIColor greenColor];
    }else{
        self.message.text = @"Wrong!!!";
        self.message.textColor = [UIColor redColor];
    }
    [self setAnswerButtonEnabled: NO];

}

-(void)setAnswerButtonEnabled:(BOOL)on{
    self.answer1.enabled = on;
    self.answer2.enabled = on;
    self.answer3.enabled = on;
    self.answer4.enabled = on;
    self.nextQuestion.enabled = !on;
}

- (IBAction)newQuestion:(id)sender {
    //gets a new question and sets the text
    self.message.text = @"";
    
    NSArray *temp = [self.questionHandler getNewQuestion];
    if (temp == nil) {
        [self displayResult];
    }else{
    
    self.questionText.text = [temp objectAtIndex:0];
    [self.answer1 setTitle:[temp objectAtIndex:1] forState:normal];
    [self.answer2 setTitle:[temp objectAtIndex:2] forState:normal];
    [self.answer3 setTitle:[temp objectAtIndex:3] forState:normal];
    [self.answer4 setTitle:[temp objectAtIndex:4] forState:normal];
    
    [self setAnswerButtonEnabled:YES];
    }
}
- (IBAction)restartGame:(id)sender {
    [self.questionHandler resetGame];
    self.playANewGame.hidden = YES;
    [self newQuestion:nil];
}


-(void)displayResult{
    self.questionText.text = [self.questionHandler getRoundResult];
    self.nextQuestion.enabled = NO;
    self.playANewGame.hidden = NO;
    NSLog(@"End or round.");
}

@end










