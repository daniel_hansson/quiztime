//
//  QuestionHandler.m
//  QuizTime
//
//  Created by Daniel Hansson on 2016-02-03.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "QuestionHandler.h"

@interface QuestionHandler ()
@property (nonatomic) NSArray *questions;
@property (nonatomic) NSArray *answers;
@property (nonatomic) int currentQuestion;
@property (nonatomic) NSMutableArray *usedQuestions;
@property (nonatomic) int numberOfQuestionsPerRound;
@property (nonatomic) int correctAnswers;
@property (nonatomic) int wrongAnswers;
@property (nonatomic) int answeredQuestions;
@end

@implementation QuestionHandler

-(instancetype)init{
    self = [super init];
    if (self) {
        self.numberOfQuestionsPerRound = 5;
        self.answeredQuestions = 0;
        self.correctAnswers = 0;
        self.wrongAnswers = 0;
        self.usedQuestions = [[NSMutableArray alloc]initWithCapacity:10];
        
        self.questions = @[@[@"Fill in:\rCATS: ALL YOUR...", @"BASES BELONG TO US.", @"BASES BELONGING TO ME.",
                             @"BASE ARE BELONG TO US.", @"BASE IS MINE."],
                           @[@"Which is not a Steve Jackson Game?", @"Chez Geek", @"Fluxx", @"Illuminati", @"Munchkin"],
                           @[@"Find the non greek god.", @"Ares", @"Dionysus", @"Hermes", @"Thor"],
                           @[@"In which book is Zaphod Beeblebrox in?", @"Hitchhikers guide to the galaxy", @"Dune", @"i,Robot", @"Solaris"],
                           @[@"Color RGB (193,18,9) is...", @"Black", @"Green", @"Red", @"Azure"],
                           @[@"YouTube was not founded by:", @"Chad Hurley", @"Larry Page", @"Steve Chen", @"Jawed Karim"],
                           @[@"The Pukaki River is in...", @"New Zealand", @"Taiwan", @"India", @"Bangladesh"],
                           @[@"What is the highest number arc4random() returns?", @"4", @"4264960295", @"4187987653", @"4294967295"],
                           @[@"Who spent time\rChasing the Whale\r?", @"Captain Bildad", @"Captain Ahab", @"Captain Peleg", @"Captain Starbuck"],
                           @[@"Find the non geek god.", @"Steve Wozniak", @"Bill Gates", @"Phil Collins", @"Steve Jobs"]];
        
        self.answers = @[@"BASE ARE BELONG TO US.",
                         @"Fluxx",
                         @"Thor",
                         @"Hitchhikers guide to the galaxy",
                         @"Red",
                         @"Larry Page",
                         @"New Zealand",
                         @"4294967295",
                         @"Captain Ahab",
                         @"Phil Collins"];
    }
    return self;
}
-(NSArray*)getNewQuestion{
    if (self.usedQuestions.count == self.numberOfQuestionsPerRound) {
        return nil;
    }
    int indexValue;
    do
    {
        indexValue = arc4random() % self.questions.count;
        NSLog(@"Gets random ralue: %d",indexValue);
    } while ([self.usedQuestions containsObject:[NSNumber numberWithInt:indexValue]]);
    NSLog(@"Value was not used before");
    
    self.currentQuestion = indexValue;
    
    [self.usedQuestions addObject:[NSNumber numberWithInt:indexValue]];
    
    return [self.questions objectAtIndex:indexValue];
}

-(BOOL)isAnswerCorrect:(NSString*)answer{
    self.answeredQuestions++;
    if ([answer isEqualToString:[self.answers objectAtIndex:self.currentQuestion]]) {
        self.correctAnswers++;
        return YES;
    }
    self.wrongAnswers++;
    return NO;
}

-(NSString*)getRoundResult{
    NSString *feedback;
    if (self.correctAnswers == self.numberOfQuestionsPerRound) {
        feedback = @"_-*AMAZING*-_";
    }else if (self.correctAnswers>self.wrongAnswers) {
        feedback = @"Good work!";
    }else{
        feedback = @"Not so well done...";
    }
    return [NSString stringWithFormat:@"RESULT:\rCorrect: %d Wrong: %d\r%@", self.correctAnswers, self.wrongAnswers, feedback];
}

-(void)resetGame{
    self.correctAnswers = 0;
    self.wrongAnswers = 0;
    self.answeredQuestions = 0;
    [self.usedQuestions removeAllObjects];
}

@end
