//
//  QuestionHandler.h
//  QuizTime
//
//  Created by Daniel Hansson on 2016-02-03.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestionHandler : NSObject

-(NSArray*)getNewQuestion;

-(BOOL)isAnswerCorrect:(NSString*)answer;

-(NSString*)getRoundResult;

-(void)resetGame;

@end
